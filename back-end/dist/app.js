"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const { Client } = require('pg');
const app = express();
const port = 3001;
app.get('/', (req, res) => {
    res.send('The sedulous hyena ate the antelope!');
});
app.listen(port, err => {
    if (err) {
        return console.error(err);
    }
    return console.log(`server is listening on ${port}`);
});
app.use(bodyParser.json());
app.use(cookieParser());
//connecting the database
const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'simpleWeb',
    password: 'dvp Himash1997',
    port: 5432,
});
client.connect();
const users = require('./routes/users.route');
app.use('/users', users);
//# sourceMappingURL=app.js.map