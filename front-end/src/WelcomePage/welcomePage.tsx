import React,{ FC }  from 'react';

import './welcomePage.css';
import ReactDOM from 'react-dom';
import Travel from '../assets/travel.jpg';
import { Form, Input, Button, Checkbox } from 'antd';
import 'antd/dist/antd.css';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 10, span: 15 },
};

const Login = () => {
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };



    return(
      <>
      <div style={{height:"15%"}}></div>
      <h1 className="title">Login</h1>
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                   
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input placeholder="E-mail"/>
                </Form.Item>

                <Form.Item
                    
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password placeholder="Password" />
                </Form.Item>

                <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit" block >
                                               Login
                    </Button>
                </Form.Item>
            <label style={{alignContent:'center'}}>or</label>
            <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="reset" block style={{backgroundColor:"green"}}>
                                               Register
                    </Button>
                </Form.Item>
            </Form></>
    )
}

const WelcomePage:FC=(props)=>{
 return(
     <>
     <div className="row">
     <img className="bgImage" src={Travel} />
         <div className="login"><Login /></div>
    </div>
     </>
 
 )
}


export default WelcomePage;
