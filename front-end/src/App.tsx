import React, { FC } from 'react';

import './App.css';
// import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import WelcomePage from './WelcomePage/welcomePage';
// type Props={name:string};

const App:FC=(props)=>{

 return(
  <BrowserRouter>
  <Switch>
      <Route path = "/" component = {WelcomePage} />
  </Switch>
  </BrowserRouter>
 )
}
export default App;
